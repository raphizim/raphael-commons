package li.raphael.commons.injection;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;

/**
 * The injection utility class has the single purpose to make the injector
 * instance globally available through all project. In most cases, the injector
 * is not required. But in some cases, if another frameworks create an instance,
 * the injector instance must be accessible in another way. Therefore, this
 * utility class privdes access to it as well as some shortcuts.
 * 
 * @author Raphael Zimmermann
 * 
 */
public final class InjectionUtils {

	/**
	 * The staticly hold instance of the injector.
	 */
	private static Injector injector;

	/**
	 * Gets the current instance of the injector. If the injector is not yet
	 * assigned, a {@link NullPointerException} is thrown.
	 * 
	 * @return the current instance of the injector.
	 */
	public static Injector getInjector() {
		if (injector == null) {
			throw new NullPointerException(
					"Injector is NULL since it is not initialized properly!");
		}
		return injector;
	}

	/**
	 * Shortcut for <code>Guice.createInjector</code>. Creates an injector with
	 * the given modules and assigns it to the static injector instance.
	 * 
	 * @param inModules
	 *            the modules to initialize. See {@link Guice} for more details.
	 * @return the newly created {@link Injector} instance.
	 */
	public static Injector createInjector(final Module... inModules) {
		injector = Guice.createInjector(inModules);
		return injector;
	}

	/**
	 * Private constructor since this class should never be instantiated.
	 */
	private InjectionUtils() {
		super();
	}

}
