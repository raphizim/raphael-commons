package li.raphael.commons.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Utility class providing helper for resources.
 * 
 * @author Raphael Zimmermann
 * 
 */
public final class ResourceUtils {

	/**
	 * Gets the string resource.
	 * 
	 * @param inContext
	 *            the context class (search relatively from this resource
	 *            package).
	 * @param inResourceName
	 *            the string
	 * @return the string resource
	 */
	public static String getStringResource(final Class<?> inContext,
			final String inResourceName) {

		// TODO: improve to throw better error if resource does not exist...
		try {
			InputStream stream = inContext.getResourceAsStream(inResourceName);
			return streamToString(stream);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Reads the given stream and writes it into a string.
	 * 
	 * @param inputStream
	 *            the stream to read.
	 * @return the result as string.
	 * @throws IOException
	 *             if the stream can not be read.
	 */
	private static String streamToString(final InputStream inputStream)
			throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		StringBuilder builder = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			builder.append(line);
			builder.append('\n');
		}
		return builder.toString();
	}

	/**
	 * Default constructor initializing the object.
	 */
	private ResourceUtils() {
		super();
	}
}
