package li.raphael.commons.exceptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * TODO: What about old threads? How to clean them up?
 * 
 * @author Raphael Zimmermann
 * 
 */
// TODO: clean me up!
public final class ExecutionContext {

	/**
	 * A map of threads linked to the requests that are executed in them. This
	 * is required to get the the requestID of the current thread.
	 */
	private static Map<Thread, String> requestIds = new ConcurrentHashMap<Thread, String>();

	/**
	 * A map taking a requestID as key and stores a list of messages as value.
	 * See log method.
	 */
	private static Map<String, List<String>> contexts = new ConcurrentHashMap<String, List<String>>();

	/**
	 * Sets the requstID of the current tread.
	 * 
	 * @param inRequestId
	 *            the requestID to set.
	 */
	public static void setRequestIdForThread(final String inRequestId) {
		requestIds.put(Thread.currentThread(), inRequestId);
	}

	/**
	 * Gets the requestID of the current tread.
	 * 
	 * @return the request ID.
	 */
	public static String getRequestIdForThread() {
		return requestIds.get(Thread.currentThread());
	}

	/**
	 * "Logs" the given message. The messages are for debug purposes and will
	 * only show up in the logs if a requests fails.
	 * 
	 * @param message
	 *            the message to log (String.format is performed on the string
	 *            taking the given args as parameters)
	 * @param args
	 *            arguments to pass to the message
	 */
	public static void log(final String message, final Object... args) {
		String requestId = getRequestIdForThread();

		List<String> context = contexts.get(requestId);

		if (context == null) {
			context = new ArrayList<String>();
			contexts.put(requestId, context);
		}

		contexts.get(requestId).add(String.format(message, args));
	}

	/**
	 * Returns all context messages for the current thread/request.
	 * 
	 * @return the context messages.
	 */
	public static List<String> getContext() {
		return getContext(getRequestIdForThread());
	}

	/**
	 * Returns all context messages for the given request.
	 * 
	 * @param inRequestId
	 *            the request id to get the messages for.
	 * @return the context messages.
	 */
	public static List<String> getContext(final String inRequestId) {
		return contexts.get(inRequestId);
	}

	/**
	 * Clears the context of the context of the current request.
	 */
	public static void clearContext() {
		clearContext(getRequestIdForThread());
	}

	/**
	 * Clears the context of the given request.
	 * 
	 * @param inRequestId
	 *            the id of the request to clear the context for.
	 */
	public static void clearContext(final String inRequestId) {
		List<String> aContext = contexts.get(inRequestId);
		if (aContext != null) {
			aContext.clear();
		}
	}

	/**
	 * Private constructor.
	 */
	private ExecutionContext() {
		super();
	}
}
