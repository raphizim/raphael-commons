package li.raphael.commons.exceptions;

/**
 * The Class ApplicationException.
 */
// TODO: clean up
// TODO: make similar to the service Exception
public abstract class ApplicationException extends Exception {

	/**
	 * Serial version id.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * The error info.
	 */
	private ErrorInfo errorInfo = null;

	/**
	 * Instantiates a new application exception.
	 * 
	 * @param inErrorInfo
	 *            the in error info
	 */
	public ApplicationException(final ErrorInfo inErrorInfo) {
		errorInfo = inErrorInfo;
	}

	/**
	 * Instantiates a new application exception.
	 * 
	 * @param inErrorInfo
	 *            the in error info
	 * @param inMessage
	 *            the in message
	 */
	public ApplicationException(final ErrorInfo inErrorInfo,
			final String inMessage) {
		super(inMessage);
		errorInfo = inErrorInfo;
	}

	/**
	 * Instantiates a new application exception.
	 * 
	 * @param inErrorInfo
	 *            the in error info
	 * @param inMessage
	 *            the in message
	 * @param inCause
	 *            the in cause
	 */
	public ApplicationException(final ErrorInfo inErrorInfo,
			final String inMessage, final Throwable inCause) {
		super(inMessage, inCause);
		errorInfo = inErrorInfo;
	}

	/**
	 * Instantiates a new application exception.
	 * 
	 * @param inErrorInfo
	 *            the in error info
	 * @param inCause
	 *            the in cause
	 */
	public ApplicationException(final ErrorInfo inErrorInfo,
			final Throwable inCause) {
		super(inCause);
		errorInfo = inErrorInfo;
	}

	/**
	 * Gets the error info.
	 * 
	 * @return the error info
	 */
	public ErrorInfo getErrorInfo() {
		return errorInfo;
	}

	@Override
	public String getMessage() {
		StringBuilder aStringBuilder = new StringBuilder();

		// Append message if defined
		String aMessage = super.getMessage();
		if (aMessage != null) {
			aStringBuilder.append(aMessage);
			aStringBuilder.append(": ");
		} // if

		// Append the Error info
		aStringBuilder.append(errorInfo.toString());

		return aStringBuilder.toString();
	}
}
