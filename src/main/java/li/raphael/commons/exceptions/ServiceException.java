package li.raphael.commons.exceptions;

//TODO: integrate String format!
//TODO: clean up
public class ServiceException extends ApplicationException {

	private static final long serialVersionUID = -1;

	public ServiceException(ErrorInfo inErrorInfo, Throwable inCause,
			String inMessage, Object... params) {
		super(inErrorInfo, String.format(inMessage, params), inCause);

	}

	public ServiceException(ErrorInfo inErrorInfo, String inMessage,
			Object... params) {
		super(inErrorInfo, String.format(inMessage, params));

	}

	public ServiceException(ErrorInfo inErrorInfo, Throwable inCause) {
		super(inErrorInfo, inCause);

	}

	public ServiceException(ErrorInfo inErrorInfo) {
		super(inErrorInfo);

	}

}
