package li.raphael.commons.exceptions;

/**
 * The Severity of an error/exception.
 * 
 * @author Raphael Zimmermann
 * 
 */
public enum Severtiy {
	/**
	 * Very critical, the application might run stable anymore.
	 */
	FATAL,
	/**
	 * Unexpected problem. Might block an issue/service, but the application
	 * remains in a stable state.
	 */
	ERROR,

	/**
	 * An unexpected or not recommended usage of a service/method etc.
	 */
	WARNING,

	/**
	 * A trivial issue, only for information purpose.
	 */
	INFO;
}
