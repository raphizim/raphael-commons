package li.raphael.commons.exceptions.template;


import li.raphael.commons.exceptions.ServiceException;

/**
 * A repeatable action is something like sending an email. It might fail for IO
 * reasons a a temporary problem of the mail service.
 * 
 * This interface can be implemented from such an action and then be scheduled
 * by for example a {@link ServiceRequestor}.
 * 
 * @author Raphael Zimmermann
 * 
 */
public interface RepeatableServiceAction {

	/**
	 * Executes the action itself.
	 * 
	 * @throws ServiceException
	 *             if the execution fails for some reason.
	 */
	void execute() throws ServiceException;

	/**
	 * This method is called if the action has failed to be executed several
	 * times. This allows the action to take alternative steps, for example in
	 * case of an email to put it temporarily on the file system.
	 * 
	 * @throws ServiceException
	 *             if the temporary preservation fails as well.
	 */
	void failed() throws ServiceException;
}
