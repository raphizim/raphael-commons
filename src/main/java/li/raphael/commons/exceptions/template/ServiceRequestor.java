package li.raphael.commons.exceptions.template;


import li.raphael.commons.exceptions.ExecutionContext;
import li.raphael.commons.exceptions.IOErrors;
import li.raphael.commons.exceptions.ServiceException;

/**
 * The {@link li.raphael.commons.exceptions.template.ServiceRequestor} class tries to execute the given action. If the
 * action fails, it can be executed again.
 * 
 * @author Raphael Zimmermann
 * 
 */
public class ServiceRequestor {
	/**
	 * TODO: extract! The number of attempts to try the action.
	 */
	private static final int ATTEMPTS = 5;

	/**
	 * TODO: extract! The timeout to wait before re-trying.
	 */
	private static final int TIMEOUT = 1000;

	/**
	 * The action to schedule.
	 */
	private RepeatableServiceAction action;

	/**
	 * Default constructor initializing the object.
	 * 
	 * @param inAction
	 *            the action to schedule.
	 */
	public ServiceRequestor(final RepeatableServiceAction inAction) {
		action = inAction;
	}

	/**
	 * Tries to execute the defined action.
	 * 
	 * @throws ServiceException
	 *             if the execution fails multiple times.
	 */
	public void execute() throws ServiceException {
		// TODO: migrate loop to java 8
		Exception problem = null;
		for (int i = 1; i <= ATTEMPTS; i++) {

			try {

				ExecutionContext.log("Attempt #%s to execute action", i);

				action.execute();

				ExecutionContext.log("Successfully executed action.");

				return;

			} catch (ServiceException e) {
				problem = e;

				// TODO: pass error message
				ExecutionContext.log(
						"Exception catched...will re-try in %s ms", TIMEOUT);
				try {

					Thread.sleep(TIMEOUT);

				} catch (InterruptedException ex) {
					// TODO: pass the exception somehow...
					ExecutionContext.log("Re-Try has failed!");
				}
			}
		}

		// Call the failed method to allow the action to take any steps to save
		// the current state elsewhere
		action.failed();

		// explode
		throw new ServiceException(IOErrors.TEMPORARY_NOT_AVAILABLE,
				String.format(
						"Action %s could not be executed after %s attempts",
						action, ATTEMPTS), problem);
	}
}
