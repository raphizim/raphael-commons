package li.raphael.commons.exceptions;

/**
 * The Class ErrorInfo is the core of the exception handling.
 */
public class ErrorInfo {

	/**
	 * The error id.
	 */
	private String errorId = null;

	/**
	 * The description.
	 */
	private String description = null;

	/**
	 * The severtiy.
	 */
	private Severtiy severtiy = null;

	/**
	 * Instantiates a new error info.
	 * 
	 * @param inErrorId
	 *            the error id
	 * @param inDescription
	 *            the description
	 * @param inSevertiy
	 *            the severtiy
	 */
	public ErrorInfo(final String inErrorId, final String inDescription,
			final Severtiy inSevertiy) {
		super();
		this.errorId = inErrorId;
		this.description = inDescription;
		this.severtiy = inSevertiy;
	}

	/**
	 * Gets the error id.
	 * 
	 * @return the error id
	 */
	public String getErrorId() {
		return errorId;
	}

	/**
	 * Gets the description.
	 * 
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the severtiy.
	 * 
	 * @return the severtiy
	 */
	public Severtiy getSevertiy() {
		return severtiy;
	}

	@Override
	public String toString() {
		return String.format("[%s] %s: %s", severtiy, errorId, description);
	}
}
