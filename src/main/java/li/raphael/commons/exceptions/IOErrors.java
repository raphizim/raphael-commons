package li.raphael.commons.exceptions;

/**
 * A set of common IO errors.
 * 
 * @author Raphael Zimmermann
 * 
 */
public final class IOErrors {

	/**
	 * A typical connection timeout.
	 */
	public static final ErrorInfo CONNECTION_TIMEOUT = new ErrorInfo(
			"ConnectionTimeout",
			"The request could not be proceeded since the connection to the backend system timed out.",
			Severtiy.WARNING);

	/**
	 * If multiple attempts attempts have failed.
	 */
	public static final ErrorInfo TEMPORARY_NOT_AVAILABLE = new ErrorInfo(
			"TemporaryNotAvailable",
			"The service is not accessible after several attempts. There is nothing the application can do. ",
			Severtiy.WARNING);

	/**
	 * Private constructor.
	 */
	private IOErrors() {
	}
}
