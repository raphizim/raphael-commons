# Raphael Commons
Raphael Commons is a BSD licensed utility library, written in Java, providing common utilities to work with.

## Features
* Effective Exception handling built in
* Depencency Injection utilities
* Various utilities

## Installation
The artifact will be available via the central maven repository.

### Do it manually (takes only a few seconds...(
Ensure java > 1.7 is running on your system, for example:

    $ java -version
    java version "1.7.0_51"
    ...

Since it will generate a maven artifact, you should also have maven installed

    $ mvn --version
    Apache Maven 3.0.5
    ...

Now clone the project

    $ git clone https://bitbucket.org/raphizim/raphael-commons.git
    # change in the the project folder
    $ cd raphael-commons/

../and build it (on windows, use *./gradlew.bat*)

    $ ./gradlew -q install

The artifact is now in your local MVN repository (~/.m2/repository/li/raphael/commons/raphael-commons) .
You can use it now with mvn:

    <dependency>
         <groupId>li.raphael.commons</groupId>
          <artifactId>raphael-commons</artifactId>
          <version>0.1.0</version>
    </dependency>

...or use it with gradle:

	compile 'li.raphael.commons:raphael-commons:0.1.0'

**Note that version might have changed!**

## Documentation
Better documentation wil follow soon. For the time being, you have to checkout the java docs.

## Contribute

1. Check for open issues or open a fresh issue to start a discussion around a feature idea or a bug.
2. Fork the repository to start making your changes to the **master** branch (or branch off of it).
3. Write a test which shows that the bug was fixed or that the feature works as expected.
4. Send a pull request and bug the maintainer until it gets merged and published. :)
5. Make sure to add yourself to Contributors.

## Contributors
* Raphael Zimmermann
